#!/usr/bin/bash
rosservice call /reset
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,-4.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-2.0,4.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,-4.0,0.0]' '[0.0,0.0,0.0]'
rosservice call turtle1/set_pen 255 0 0 5 1
rosservice call /turtle1/teleport_relative 4 0
rosservice call /turtle1/set_pen 255 0 0 5 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[8.0,0.0,0.0]' '[0.0,0.0,7.2]'
