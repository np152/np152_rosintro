#!/usr/bin/env python
# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("this_stuff", anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# We get the joint values from the group and change some of the values:
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8
joint_goal[2] = -tau / 4
joint_goal[3] = 0
joint_goal[4] = 0
joint_goal[5] = tau / 6  # 1/6 of a turn

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

#function that moves the EE to a desired cartesian coord in xyz 
def location(x,y,z):
	pose_goal = geometry_msgs.msg.Pose()
	pose_goal.orientation.w = 1.0
	pose_goal.position.x = x
	pose_goal.position.y = y
	pose_goal.position.z = z

	move_group.set_pose_target(pose_goal)
	success = move_group.go(wait=True)
	move_group.stop()
	move_group.clear_pose_targets()

#start positon
location(.09,.19,.5)
# up for N
location(.09,.19,.65)
location(.09,.19,.8)
# diagonal for N
location(.09,.303,.65)
location(.09,.415,.5)
# up to finish N 
location(.09,.415,.65)
location(.09,.415,.8)
#back to start
location(.09,.19,.5)
#up for M 
location(.09,.19,.65)
location(.09,.19,.8)
#diagonal down
location(.09,.303,.65)
#diagonal up
location(.09,.415,.8)
#down to complete M
location(.09,.415,.65)
location(.09,.415,.5)
#back to start
location(.09,.19,.5)
#up for P 
location(.09,.19,.8)
#p wrap
location(.09,.285,.75)
location(.09,.303,.725)
location(.09,.285,.70)
location(.09,.235,.65)
location(.09,.19,.65)
