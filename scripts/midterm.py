#!/usr/bin/env python
# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("this_stuff", anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# We get the joint values from the group and change some of the values:
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8
joint_goal[2] = -tau / 4
joint_goal[3] = 0
joint_goal[4] = 0
joint_goal[5] = tau / 6  # 1/6 of a turn

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.09
pose_goal.position.y = 0.19
pose_goal.position.z = 0.5

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#now setup  makes first up motion for N
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.19
pose_goal.position.z = 0.8

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
#diagonal motion down

pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.415
pose_goal.position.z = 0.5

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#last up motion
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.415
pose_goal.position.z = 0.8

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#return to starting position to begin M

pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.09
pose_goal.position.y = 0.19
pose_goal.position.z = 0.5

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#first up motion on M
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.19
pose_goal.position.z = 0.8

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#half diagonal down
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.303
pose_goal.position.z = 0.65

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#diagonal back up 
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.415
pose_goal.position.z = 0.8

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#vetical down to complete M 
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.415
pose_goal.position.z = 0.5

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#return to start position for P
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.09
pose_goal.position.y = 0.19
pose_goal.position.z = 0.5

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#up for P
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.09
pose_goal.position.y = 0.19
pose_goal.position.z = 0.8

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#start motion for rest
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.303
pose_goal.position.z = 0.8

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()


pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.303
pose_goal.position.z = 0.65

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()


pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0899
pose_goal.position.y = 0.19
pose_goal.position.z = 0.65

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

